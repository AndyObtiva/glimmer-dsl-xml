# Change Log

## 0.2.0

- Relaxed dependency on glimmer

## 0.1.0

- Extracted Glimmer DSL for SWT (glimmer-dsl-swt gem) from Glimmer
